import torch.nn as nn
import numpy as np
from globalvar import *
import torch
from mainloop import device
from torch.autograd import Variable
from torch.nn import TripletMarginLoss


class TripletLoss(nn.Module):
    def __init__(self, margin=1):
        super(TripletLoss, self).__init__()
        self.margin = margin
        self.ranking_loss = nn.MarginRankingLoss(margin=self.margin)  # 获得一个简单的距离triplet函数

    def forward(self, inputs, labels):
        n = inputs.size(0)  # 获取batch_size
        # Compute pairwise distance, replace by the official when merged
        dist = torch.pow(inputs, 2).sum(dim=1, keepdim=True).expand(n, n)  # 每个数平方后， 进行加和（通过keep_dim保持2维），再扩展成nxn维
        dist = dist + dist.t()  # 这样每个dis[i][j]代表的是第i个特征与第j个特征的平方的和
        dist.addmm_(1, -2, inputs, inputs.t())  # 然后减去2倍的 第i个特征*第j个特征 从而通过完全平方式得到 (a-b)^2
        dist = dist.clamp(min=1e-12).sqrt()  # 然后开方
        # For each anchor, find the hardest positive and negative
        mask = labels.expand(n, n).eq(labels.expand(n, n).t())  # 这里dist[i][j] = 1代表i和j的label相同， =0代表i和j的label不相同
        dist_ap, dist_an = [], []
        for i in range(n):
            dist_ap.append(dist[i][mask[i]].max().unsqueeze(0))  # 在i与所有有相同label的j的距离中找一个最大的
            dist_an.append(dist[i][mask[i] == 0].min().unsqueeze(0))  # 在i与所有不同label的j的距离找一个最小的
        dist_ap = torch.cat(dist_ap)  # 将list里的tensor拼接成新的tensor
        dist_an = torch.cat(dist_an)
        # Compute ranking hinge loss
        y = torch.ones_like(dist_an)  # 声明一个与dist_an相同shape的全1tensor
        # c = get_c_value()
        # ranking_loss = nn.MarginRankingLoss(margin=c)  # 获得一个简单的距离triplet函数
        loss = self.ranking_loss(dist_an, dist_ap, y)
        return loss


class MSMLloss(nn.Module):
    def __init__(self, margin=1):
        super(MSMLloss, self).__init__()
        self.margin = margin
        self.ranking_loss = nn.MarginRankingLoss(margin=self.margin)  # 获得一个简单的距离triplet函数

    def forward(self, inputs, labels):
        ap = torch.tensor([0]).float()  # 初始數值給小
        an = torch.tensor([999999]).float()  # 初始數值給大
        for i in range(inputs.size(0)):
            for j in range(inputs.size(0)):
                if i < j:
                    if labels[i] == labels[j]:  # 正樣本
                        if torch.dist(inputs[i], inputs[j], p=2).item() > ap.item():
                            ap = torch.dist(inputs[i], inputs[j], p=2)  # 用來與前一個比較
                            # ap_dist = torch.dist(inputs[i], inputs[j], p=2).unsqueeze(0)
                            a = inputs[i]
                            p = inputs[j]
                    else:  # 負樣本
                        if torch.dist(inputs[i], inputs[j], p=2).item() < an.item():
                            an = torch.dist(inputs[i], inputs[j], p=2)  # 用來與前一個比較
                            # an_dist = torch.dist(inputs[i], inputs[j], p=2).unsqueeze(0)
                            m = inputs[i]
                            n = inputs[j]
        # ap = Variable(torch.tensor([max(dist_ap)]))
        # an = Variable(torch.tensor([max(dist_an)]))
        # from mainloop2 import batchSize
        # ap_dist = ap_dist.repeat(1, batchSize)
        # an_dist = an_dist.repeat(1, batchSize)
        y = Variable(torch.ones_like(ap.unsqueeze(0)))  # 声明一个与an相同shape的全1tensor
        # c = get_c_value()
        # ranking_loss = nn.MarginRankingLoss(margin=c)  # 获得一个简单的距离triplet函数
        loss = self.ranking_loss(an.unsqueeze(0), ap.unsqueeze(0), y)
        return loss


class myLoss(nn.Module):
    def __init__(self, margin=1.0):
        super(myLoss, self).__init__()
        self.margin = margin
        self.ranking_loss = nn.MarginRankingLoss(margin=self.margin)  # 获得一个简单的距离triplet函数
        self.tripletloss = TripletMarginLoss(margin=margin, p=2)
    def forward(self, inputs, labels):
        ap = 0  # 初始數值給小
        an = 99999  # 初始數值給大
        for i in range(inputs.size(0)):
            ap_dist = []
            an_dist = []
            for j in range(inputs.size(0)):
                if i < j:
                    if labels[i] == labels[j]:  # 正樣本
                        if torch.dist(inputs[i], inputs[j], p=2).item() > ap:
                            ap = torch.dist(inputs[i], inputs[j], p=2).item()  # 用來與前一個比較
                            ap_dist = torch.dist(inputs[i], inputs[j], p=2).unsqueeze(0)
                            a = inputs[i]
                            a_lable = labels[i]
                            p = inputs[j]
                    # else:  # 負樣本
                    #     an_dist.append(torch.dist(inputs[i], inputs[j], p=2).item())
                        # if torch.dist(inputs[i], inputs[j], p=2).item() < an:
                        #     an = torch.dist(inputs[i], inputs[j], p=2).item()  # 用來與前一個比較
                        #     an_dist = torch.dist(inputs[i], inputs[j], p=2).unsqueeze(0)
                        #     m = inputs[i]
                        #     n = inputs[j]
        for i in range(inputs.size(0)):
            if labels[i] != a_lable:
                if (torch.dist(a, inputs[i], p=2).item()) < an:
                    an = torch.dist(a, inputs[i], p=2).item()
                    n = inputs[i]
        a = a.unsqueeze(0)
        p = p.unsqueeze(0)
        n = n.unsqueeze(0)
        loss = self.tripletloss(a, p, n)
        return loss


def tripletValidation(outputs, labels):
    dist_ap, dist_an = [], []
    for i in range(outputs.size(0)):
        for j in range(outputs.size(0)):
            if i < j:
                if labels[i] == labels[j]:
                    dist_ap.append((torch.dist(outputs[i], outputs[j], p=2).item()))
                    # print(torch.sqrt(torch.sum((outputs[i]-outputs[j])**2)))
                    # print((torch.dist(outputs[i], outputs[j], p=2).item()))
                else:
                    dist_an.append((torch.dist(outputs[i], outputs[j], p=2).item()))

    # print('dist_ap  ' + str(dist_ap))
    print('Average of ap = ' + str(np.mean(dist_ap)))
    # print('dist_an  ' + str(dist_an))
    print('Average of an = ' + str(np.mean(dist_an)))
    # ap = sum(i < (np.mean(dist_ap)+((np.mean(dist_an)-np.mean(dist_ap)) * 0.2)) for i in dist_ap)
    # an = sum(i > (np.mean(dist_ap)-((np.mean(dist_an)-np.mean(dist_ap)) * 0.2)) for i in dist_an)
    # ap_correct = ap / len(dist_ap)
    # an_correct = an / len(dist_an)
    # c = get_c_value()
    #
    # if np.mean(dist_ap) + c < np.mean(dist_an):
    #     set_c_value()
    #     print('Raise the c to ' + str(get_c_value()))



    # n = outputs.size(0)  # 获取batch_size
    # # Compute pairwise distance, replace by the official when merged
    # dist = torch.pow(outputs, 2).sum(dim=1, keepdim=True).expand(n, n)  # 每个数平方后， 进行加和（通过keep_dim保持2维），再扩展成nxn维
    # dist = dist + dist.t()  # 这样每个dis[i][j]代表的是第i个特征与第j个特征的平方的和
    # dist.addmm_(1, -2, outputs, outputs.t())  # 然后减去2倍的 第i个特征*第j个特征 从而通过完全平方式得到 (a-b)^2
    # dist = dist.clamp(min=1e-12).sqrt()  # 然后开方
    # # For each anchor, find the hardest positive and negative
    # mask = labels.expand(n, n).eq(labels.expand(n, n).t())  # 这里dist[i][j] = 1代表i和j的label相同， =0代表i和j的label不相同
    #
    #
    # for i in range(n):
    #     if len(dist[i][mask[i]]) > 1:
    #         dist_ap.append(dist[i][mask[i]].max().unsqueeze(0))  # 在i与所有有相同label的j的距离中找一个最大的
    #         set_ap_value(dist[i][mask[i]].max().unsqueeze(0))
    #     # batch如果太小 剛好得到一樣的 labels 會找不到 negative 會出錯
    #     dist_an.append(dist[i][mask[i] == 0].min().unsqueeze(0))  # 在i与所有不同label的j的距离找一个最小的
    #     set_an_value(dist[i][mask[i] == 0].min().unsqueeze(0))
    # print('ap')
    # print(dist_ap)
    # print('an')
    # print(dist_an)
    # from mainloop import device
    # t1 = torch.tensor([0.]).to(device)
    # t2 = torch.tensor([0.]).to(device)
    # for i in dist_ap:
    #     t1 =torch.cat((t1,i)).to(device)
    # for j in dist_an:
    #     t2 = torch.cat((t2, j)).to(device)
    # if t1.mean().item() +1 < t2.mean().item():
    #     correct = 1
    # else:
    #     correct = 0

    # return ap_correct, an_correct
    return 100,100