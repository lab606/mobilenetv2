import numpy as np
import torch
from torchvision import datasets, transforms
from torch.utils.data.sampler import SubsetRandomSampler
from utils import plot_images
import random


class dataset():

    def __init__(self, train_data_dir, test_data_dir):
        self.train_data_dir = train_data_dir
        self.test_data_dir = test_data_dir
        self.normalize = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

        self.data_transforms = {
            'train':
            transforms.Compose([
                transforms.RandomResizedCrop(128),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                self.normalize
            ]),
            'val':
            transforms.Compose([
                transforms.Resize(128),
                transforms.CenterCrop(128),
                # transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                self.normalize
            ]),
            'test':
            transforms.Compose([
                transforms.RandomResizedCrop(128),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                self.normalize
            ])
        }

        self.train_dataset = datasets.ImageFolder(root=self.train_data_dir, transform=self.data_transforms['train'])
        self.valid_dataset = datasets.ImageFolder(root=self.train_data_dir, transform=self.data_transforms['val'])
        self.test_dataset = datasets.ImageFolder(root=self.test_data_dir, transform=self.data_transforms['test'])


    def get_train_valid_loader(self,
                               batch_size,
                               random_seed,
                               num_k_valid=None,
                               k_valid=False,
                               multiplier_k_valid=None,
                               valid_size=0.1,
                               shuffle=True,
                               show_sample=False,
                               num_workers=1,
                               pin_memory=True
                               ):
        """
        Utility function for loading and returning train and valid
        A sample 9x9 grid of the images can be optionally displayed.
        If using CUDA, num_workers should be set to 1 and pin_memory to True.

        Params
        ------
        - k_valid:use k_fold_cross_validation or not
        - num_k_valid:how many times of split of data set
            default:0 --> it will cause an error,please set
        - multiplier_k_valid: parameter of num_k_valid
            default:0 --> it will cause an error
                          please set it in a loop
            ex. USE a loop in training like this:
                for i in range(1, num_k_valid+1)
                then set multiplier_k_valid=i
        -------
        - data_dir: path directory to the data set.
        - batch_size: how many samples per batch to load.
        - augment: whether to apply the data augmentation scheme
          mentioned in the paper. Only applied on the train split.
        - random_seed: fix seed for reproducibility.
        - valid_size: percentage split of the training set used for
          the validation set. Should be a float in the range [0, 1].
        - shuffle: whether to shuffle the train/validation indices.
        - show_sample: plot 9x9 sample grid of the dataset.
        - num_workers: number of subprocesses to use when loading the dataset.
        - pin_memory: whether to copy tensors into CUDA pinned memory. Set it to
          True if using GPU.
        Returns
        -------
        - train_loader: training set iterator.
        - valid_loader: validation set iterator.
        """
        error_msg = "[!] valid_size should be in the range [0, 1]."
        assert ((valid_size >= 0) and (valid_size <= 1)), error_msg


        # load the dataset
        num_train = len(self.train_dataset)
        indices = list(range(num_train))
        
        indices2 = []
        dictIndex = dict(zip(indices,self.train_dataset.targets))
        f_dict = {}
        for i in set(self.train_dataset.targets):
            listIndex = []
            for (k, v) in dictIndex.items():
                if v == i:
                    listIndex.append(k)
            f_dict[i] = listIndex

        moreThanTwo = []
        all = []
        for i in f_dict.keys():
            all.append(i)
            if len(f_dict[i]) > 1:
                moreThanTwo.append(i)


        while len(moreThanTwo) > 0 and len(all) > 0:
            print(len(moreThanTwo),len(all))
            sampleTarget1 = random.sample(moreThanTwo, 1)
            sampleTarget2 = random.sample(all, 1)
            print(sampleTarget1[0],sampleTarget2[0])
            if sampleTarget1 == sampleTarget2:
                if len(moreThanTwo)==len(all)==1:
                    break
                continue
            if len(f_dict[sampleTarget2[0]]) < 1:
                all.remove(sampleTarget2[0])
                continue
            if len(f_dict[sampleTarget1[0]]) < 2:
                moreThanTwo.remove(sampleTarget1[0])
                continue
            sampleIndex1 = random.sample(f_dict[sampleTarget1[0]], 2)
            sampleIndex2 = random.sample(f_dict[sampleTarget2[0]], 1)

            f_dict[sampleTarget1[0]].remove(sampleIndex1[0])
            f_dict[sampleTarget1[0]].remove(sampleIndex1[1])
            f_dict[sampleTarget2[0]].remove(sampleIndex2[0])

            indices2.append(sampleIndex1[0])
            indices2.append(sampleIndex1[1])
            indices2.append(sampleIndex2[0])


        if shuffle == True:
            np.random.seed(random_seed)
            np.random.shuffle(indices)

        if k_valid == False:
            train_idx, valid_idx = indices[split:], indices[:split]
        else:
            # 使用交叉驗證 請固定random_seed
            error_msg = 'Please set static random_seed'
            error_msg2 = "Please set num_k_valid"
            error_msg3 = "Please set multiplier_k_valid"
            assert type(random_seed) == int, error_msg
            assert num_k_valid > 1, error_msg2
            assert multiplier_k_valid > 0, error_msg3

            split = int(np.floor((1/num_k_valid) * num_train))
            train_idx = indices[:split * (multiplier_k_valid-1)]
            train_idx_back = indices[split * multiplier_k_valid:]
            train_idx.extend(train_idx_back)
            valid_idx = indices[split * (multiplier_k_valid - 1):split * multiplier_k_valid]

        train_sampler = SubsetRandomSampler(train_idx)
        valid_sampler = SubsetRandomSampler(valid_idx)
        print('train_idx:' + str(len(train_sampler.indices)))
        print('val_idx:' + str(len(valid_sampler.indices)))


        train_loader = torch.utils.data.DataLoader(self.train_dataset,
                                                   batch_size=batch_size, sampler=train_sampler,
                                                   num_workers=num_workers, pin_memory=pin_memory,
                                                   drop_last=True
                                                   )

        valid_loader = torch.utils.data.DataLoader(self.valid_dataset,
                                                   batch_size=batch_size, sampler=valid_sampler,
                                                   num_workers=num_workers, pin_memory=pin_memory,
                                                   drop_last=True
                                                   )

        # visualize some images
        if show_sample:
            sample_loader = torch.utils.data.DataLoader(self.train_dataset,
                                                        batch_size=9,
                                                        shuffle=shuffle,
                                                        num_workers=num_workers,
                                                        pin_memory=pin_memory,
                                                        drop_last=True
                                                        )
            data_iter = iter(sample_loader)
            images, labels = data_iter.next()
            X = images.numpy()
            plot_images(X, labels)

        return train_loader, valid_loader


    def get_test_loader(self,
                        batch_size,
                        shuffle=True,
                        num_workers=1,
                        pin_memory=True):
        """
        Utility function for loading and returning a multi-process
        If using CUDA, num_workers should be set to 1 and pin_memory to True.
        Params
        ------
        - data_dir: path directory to the dataset.
        - batch_size: how many samples per batch to load.
        - shuffle: whether to shuffle the dataset after every epoch.
        - num_workers: number of subprocesses to use when loading the dataset.
        - pin_memory: whether to copy tensors into CUDA pinned memory. Set it to
          True if using GPU.
        Returns
        -------
        - data_loader: test set iterator.
        """


        data_loader = torch.utils.data.DataLoader(self.test_dataset,
                                                  batch_size=batch_size,
                                                  shuffle=shuffle,
                                                  num_workers=num_workers,
                                                  pin_memory=pin_memory,
                                                  drop_last=True
                                                  )


        return data_loader