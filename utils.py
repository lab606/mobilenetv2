import csv
import os
import torch
import matplotlib.pyplot as plt
from torchvision import transforms
from torchvision import datasets
import numpy as np

def plot_images(images, labels, preds=None):
    assert len(images) == len(labels) == 9

    # Create figure with sub-plots.
    fig, axes = plt.subplots(3, 3)

    for i, ax in enumerate(axes.flat):
        ax.imshow(images[i, 0, :, :], interpolation='spline16', cmap='gray')

        label = str(labels[i])
        if preds is None:
            xlabel = label
        else:
            pred = str(preds[i])
            xlabel = "True: {0}\nPred: {1}".format(label, pred)

        ax.set_xlabel(xlabel)
        ax.set_xticks([])
        ax.set_yticks([])

    plt.show()

# 獲取資料夾個數作為classes
def num_classes(path):
    count = 0
    for fn in os.listdir(path):  # fn 表示的是文件名
        count = count + 1
    return count

# 寫入csv
def csvSaver(name, data):
    with open(str(name) + '.csv', 'w') as f:
        f_csv = csv.writer(f)
        f_csv.writerow(data)

# 存圖(USELESS)
def saveGraphic(Loss_list, name):

    x2 = range(0, len(Loss_list))
    y2 = Loss_list
    plt.subplot(2, 1, 1)

    plt.plot(x2, y2, '.-')
    plt.show()
    plt.savefig(name)



