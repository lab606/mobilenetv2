import model
import torch.optim as optim
import numpy as np
from torchsummary import summary
from utils import *
from dataLoader2 import dataset2
from trip import *
from globalvar import GlobalVar
from torch.autograd import Variable
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


path = {
    'server_train': '/home/dl_learning2/lfwNew',
    'server_test': '/home/dl_learning2/VGGData/test',
    'linux_train': '/home/lab606/Desktop/ImageFolder2/train',
    'linux_test': '/home/lab606/Desktop/ImageFolder2/test',
    'mac_train': '/Users/yo/Desktop/train',
    'mac_test': '/Users/yo/Desktop/train',
    'win_train': 'C:/Users/LAB606/Desktop/lfw',
    'win_test': 'C:/Users/LAB606/Desktop/new',
    'backup': '/home/dl_learning2/backup718/'
    }

# CPU or GPU
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# set the path
now_train_path = path['win_train']
now_test_path = path['win_test']

initial_lr = 0.001
epochs = 2001
batchSize = 1
inputSize = 224


class main():

    def __init__(self, net):

        self.net = net
        self.optimizer = optim.Adam(self.net.parameters(), lr=initial_lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0)
        self.Tripletloss = TripletLoss(margin=1)
        self.MSMLloss = MSMLloss()
        self.myloss = myLoss()

    def training(self, train_loader, len_trainIndex):
            epoch_loss = 0.
            for i, (inputs, targets) in enumerate(train_loader):
                print(inputs)
                print(inputs.shape)
                inputs, targets = Variable(inputs.to(device)), Variable(targets.to(device))
                self.optimizer.zero_grad()  # 梯度歸零
                outputs = self.net(inputs).to(device)
                loss = self.Tripletloss(outputs, targets).to(device)
                # loss = self.myloss(outputs, targets).to(device)
                # loss = self.MSMLloss(outputs, targets).to(device)
                # print(loss)
                loss.backward()
                self.optimizer.step()
                epoch_loss += loss.item()
            return epoch_loss

    def validation(self, val_loader):
        an_correctlist = []
        ap_correctlist = []
        optimizer = self.optimizer
        with torch.no_grad():
            for batch_idx, (inputs, targets) in enumerate(val_loader):
                inputs, targets = inputs.to(device), targets.to(device)
                optimizer.zero_grad()  # 梯度歸零
                outputs = self.net(inputs).to(device)
                ap_correct, an_correct = tripletValidation(outputs, targets)
                ap_correctlist.append(ap_correct)
                an_correctlist.append(an_correct)
            # print('This ap_correctlist  '+str(ap_correctlist))
            # print('This an_correctlist  '+str(an_correctlist))

        return ap_correctlist, an_correctlist


if __name__ == '__main__':

    net = model.MobileNetV2(input_size=inputSize).to(device)
    summary(net, (3, 224, 224))
    main = main(net=net)
    Data = dataset2(train_data_dir=now_train_path, test_data_dir=now_test_path)
    lossList = []
    all_an_correctlist = []
    all_ap_correctlist = []
    for epoch in range(epochs):
        print('epoch = ' + str(epoch))
        trainData, len_trainIndex, valData = Data.traindata(batchSize=batchSize)
        epoch_loss = main.training(train_loader=trainData,len_trainIndex=len_trainIndex)
        lossList.append(epoch_loss)
        print(epoch_loss)
        if len(lossList) % 50 == 0:
            print('validation Start')
            print('mean_lossList = ' + str(np.mean(lossList)))
            ap_correctlist, an_correctlist = main.validation(val_loader=valData)
            # for i in an_correctlist:
            #     all_an_correctlist.append(i)
            # for i in ap_correctlist:
            #     all_ap_correctlist.append(ap_correctlist)
            # print('all_ap_correctlist' + str(all_ap_correctlist))
            # print('all_an_correctlist' + str(all_an_correctlist))
    # print('Now c(margin) is ' + str(GlobalVar.c))
    # 儲存各種東西
    csvSaver(path['backup'] + 'train_Loss_list', lossList)
    csvSaver(path['backup'] + 'all_an_correctlist', all_an_correctlist)
    csvSaver(path['backup'] + 'all_ap_correctlist', all_ap_correctlist)
    torch.save(net, 'net{}.pkl'.format(epochs))
    torch.save(net.state_dict(), 'net_params{}.pkl'.format(epochs))

