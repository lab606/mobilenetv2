import numpy as np
import torch
from torchvision import datasets, transforms
from torch.utils.data.sampler import SubsetRandomSampler, SequentialSampler
from utils import plot_images
import random, traceback

class SubsetRandomSampler_v2(SubsetRandomSampler):
    def __iter__(self):
        return (self.indices[i] for i in range(len(self.indices)))

class dataset2():

    def __init__(self, train_data_dir, test_data_dir):
        self.train_data_dir = train_data_dir
        self.test_data_dir = test_data_dir
        self.normalize = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        from mainloop2 import inputSize
        self.data_transforms = {
            'train':
                transforms.Compose([
                    # transforms.RandomResizedCrop(128),
                    transforms.Resize((inputSize, inputSize)),
                    # transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    self.normalize
                ]),
            'val':
                transforms.Compose([
                    # transforms.RandomResizedCrop(128),
                    transforms.Resize((inputSize, inputSize)),
                    # transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    self.normalize
                ]),
            'test':
                transforms.Compose([
                    # transforms.RandomResizedCrop(128),
                    transforms.Resize((inputSize, inputSize)),
                    # transforms.Resize(128,128),
                    # transforms.RandomHorizontalFlip(),
                    transforms.ToTensor(),
                    self.normalize
                ])
        }

        self.train_dataset = datasets.ImageFolder(root=self.train_data_dir, transform=self.data_transforms['train'])
        self.valid_dataset = datasets.ImageFolder(root=self.train_data_dir, transform=self.data_transforms['val'])
        self.test_dataset = datasets.ImageFolder(root=self.test_data_dir, transform=self.data_transforms['test'])


    def traindata(self, valid_size = 0.2, batchSize = 8, num_workers = 6, pin_memory = True):

        num_train = len(self.train_dataset)
        alltargets = self.train_dataset.targets
        indices = list(range(num_train))
        split = int(np.floor((1-valid_size) * (num_train)))
        # 分一個比例給validation再分別做排序
        valTarget, trainTarget  = alltargets[split:], alltargets[:split]
        valIndices, trainIndices = indices[split:], indices[:split]
        sampleindices_train = ordered(trainIndices, trainTarget)
        sampleindices_val = ordered(valIndices, valTarget)
        # 裁切驗證比例
        # split = int(np.floor((1-valid_size) * len(sampleindices)))
        # valid_idx, train_idx= sampleindices[split:], sampleindices[:split]
        train_sampler = SubsetRandomSampler_v2(sampleindices_train)
        valid_sampler = SubsetRandomSampler_v2(sampleindices_val)

        print('num of train index:{}'.format(len(sampleindices_train)))
        print('num of val index:{}'.format(len(sampleindices_val)))

        train_loader = torch.utils.data.DataLoader(dataset=self.train_dataset,
                                                    batch_size=batchSize, sampler=train_sampler,
                                                    num_workers=num_workers, pin_memory=pin_memory,
                                                    drop_last=True
                                                    )
        val_loader = torch.utils.data.DataLoader(dataset=self.valid_dataset,
                                                   batch_size=batchSize, sampler=valid_sampler,
                                                   num_workers=num_workers, pin_memory=pin_memory,
                                                   drop_last=True
                                                   )
        return train_loader,1, val_loader

    def testData(self, shuffle=True, batch_size=50, valid_size = 0.2, num_workers = 6, pin_memory = True):

        # num_train = len(self.test_dataset)
        # alltargets = self.test_dataset.targets
        # indices = list(range(num_train))
        test_loader = torch.utils.data.DataLoader(dataset=self.test_dataset,
                                                 batch_size=batch_size,
                                                 num_workers=num_workers, pin_memory=pin_memory,
                                                 drop_last=True
                                                 )
        return test_loader


def ordered(indices, alltargets):  # target 1,1,3,4,2,2,5,6 有多個單張圖片再使用
    # 資料排序
    dictIndex = dict(zip(indices, alltargets))
    f_dict = {}
    indexMoreThanTwo = []
    indexSingle = []
    sampleindices = []
    for i in set(alltargets):
        listIndex = []
        for (k, v) in dictIndex.items():
            if v == i:
                listIndex.append(k)
        f_dict[i] = listIndex
    for i in f_dict.keys():
        if len(f_dict[i]) > 1:
            indexMoreThanTwo.append(i)
        if len(f_dict[i]) == 1:
            indexSingle.append(i)
    while len(indexMoreThanTwo) > 0 and len(indexSingle) > 1:
        sampleTarget = random.sample(indexMoreThanTwo, 1)
        sampleIndex = random.sample(f_dict[sampleTarget[0]], 2)
        f_dict[sampleTarget[0]].remove(sampleIndex[0])
        f_dict[sampleTarget[0]].remove(sampleIndex[1])
        if len(f_dict[sampleTarget[0]]) < 2:
            indexMoreThanTwo.remove(sampleTarget[0])
        for i in sampleIndex:
            sampleindices.append(i)
        for i in range(2):
            sampleTargetSingle = random.sample(indexSingle, 1)
            sampleIndexSingle = random.sample(f_dict[sampleTargetSingle[0]], 1)
            f_dict[sampleTargetSingle[0]].remove(sampleIndexSingle[0])
            indexSingle.remove(sampleTargetSingle[0])
            sampleindices.append(sampleIndexSingle[0])
    return sampleindices


def ordered2(indices, alltargets):  # target 1,1,2,2,3,3
    dictIndex = dict(zip(indices, alltargets))
    f_dict = {}
    indexMoreThanTwo = []
    sampleindices = []
    for i in set(alltargets):
        listIndex = []
        for (k, v) in dictIndex.items():
            if v == i:
                listIndex.append(k)
        f_dict[i] = listIndex
    for i in f_dict.keys():
        if len(f_dict[i]) > 1:
            indexMoreThanTwo.append(i)
    while len(indexMoreThanTwo) > 0:
        sampleTarget = random.sample(indexMoreThanTwo, 1)
        sampleIndex = random.sample(f_dict[sampleTarget[0]], 2)
        f_dict[sampleTarget[0]].remove(sampleIndex[0])
        f_dict[sampleTarget[0]].remove(sampleIndex[1])
        if len(f_dict[sampleTarget[0]]) < 2:
            indexMoreThanTwo.remove(sampleTarget[0])
        for i in sampleIndex:
            sampleindices.append(i)
    return sampleindices



