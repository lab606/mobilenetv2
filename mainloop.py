import model
import torch.nn as nn
import torch.optim as optim
import traceback
from torchsummary import summary
from utils import *
from dataLoader import dataset
from statistics import mean
from trip import *
from globalvar import *

path = {
    'server_train': '/home/dl_learning2/VGGData/train1',
    'server_test': '/home/dl_learning2/VGGData/test',
    'linux_train': '/home/lab606/Desktop/ImageFolder2/train',
    'linux_test': '/home/lab606/Desktop/ImageFolder2/test',
    'mac_train': '/Users/yo/Desktop/train',
    'mac_test': '/Users/yo/Desktop/test',
    'win_train': 'C:/Users/LAB606/Desktop/new2',
    'win_test': 'C:/Users/LAB606/Desktop/new',
    'backup': '/home/dl_learning2/backup/'
    }

# CPU or GPU
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# set the path
now_train_path = path['mac_train']
now_test_path = path['mac_test']
# train parameter
initial_lr = 0.002
epoch = 601
batch_size = 10
num_workers = 6
triplet = True

# k_validation parameter
k_valid = False
num_k_valid = 10
random_seed = 666

# train or test
train = True

class main():

    def __init__(self, epoch, n_class):
        self.all_val_Accuracy_list = []
        self.n_class = n_class
        self.net = model.MobileNetV2(n_class=self.n_class, input_size=128).to(device)
        summary(self.net, (3, 128, 128))
        self.epoch = epoch
        self.optimizer = optim.Adam(self.net.parameters(), lr=initial_lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0)
        self.TripletLoss = TripletLoss().to(device)

    def training(self, train_loader, val_loader):
        print('classes = '+str(self.n_class), 'epoch = '+str(self.epoch))
        criterion = nn.CrossEntropyLoss()
        Accuracy_list = []
        Loss_list = []
        val_Accuracy_list = []

        for epoch in range(self.epoch):
            epoch_loss = 0.
            total = 0.
            correct = 0.
            print('epoch = ' + str(epoch))
            optimizer = self.optimizer
            for i, (inputs, targets) in enumerate(train_loader):
                optimizer = optim.Adam(self.net.parameters(), lr=initial_lr, betas=(0.9, 0.999), eps=1e-08,
                                       weight_decay=0)
                inputs, targets = inputs.to(device), targets.to(device)
                print(targets)
                optimizer.zero_grad()  # 梯度歸零

                outputs = self.net(inputs)
                if triplet == True:
                    loss = self.TripletLoss(outputs, targets)
                else:
                    loss = criterion(outputs, targets)
                loss.backward()
                optimizer.step()

                epoch_loss += loss.item()
                # _, predicted = torch.max(outputs.data, 1)
                total += targets.size(0)
                # correct += predicted.eq(targets).sum().item()
            # Accuracy_list.append(correct * 100 / total)
            # Loss_list.append(epoch_loss / total)
            # 獲取val_Accuracy
            if epoch != 0 and epoch % 50 == 0:
                val_Accuracy = self.validation(val_loader=val_loader)
                val_Accuracy_list.append(val_Accuracy)
                print('[%d epoch] Accuracy of the network on the validation images: %d %%' % (epoch, val_Accuracy))
            if self.epoch == (epoch+1):
                dist_ap, dist_an = get_value()
                csvSaver(path['backup'] + 'dist_ap', dist_ap)
                csvSaver(path['backup'] + 'dist_an', dist_an)

        # from second k_validation loop 判斷準確度 存準確度最大的模型
        self.all_val_Accuracy_list.append(val_Accuracy_list)
        if len(self.all_val_Accuracy_list) == 2:
            print(mean(self.all_val_Accuracy_list[0]), mean(self.all_val_Accuracy_list[1]))
            if mean(self.all_val_Accuracy_list[0]) < mean(self.all_val_Accuracy_list[1]):

                self.all_val_Accuracy_list.pop(0)

                os.remove(path['backup'] + 'net{}.pkl'.format(self.epoch))
                os.remove(path['backup'] + 'net_params{}.pkl'.format(self.epoch))
                os.remove(path['backup'] + 'train_Loss_list.csv')
                os.remove(path['backup'] + 'train_Accuracy_list.csv')
                os.remove(path['backup'] + 'val_Accuracy_list.csv')

                torch.save(self.net, 'net{}.pkl'.format(self.epoch))
                torch.save(self.net.state_dict(), 'net_params{}.pkl'.format(self.epoch))
                csvSaver(path['backup'] + 'train_Loss_list', Loss_list)
                csvSaver(path['backup'] + 'train_Accuracy_list', Accuracy_list)
                csvSaver(path['backup'] + 'val_Accuracy_list',val_Accuracy_list)

            else:
                self.all_val_Accuracy_list.pop(1)
        # 第一個 k_validation loop
        else:
            torch.save(self.net, 'net{}.pkl'.format(self.epoch))
            torch.save(self.net.state_dict(), 'net_params{}.pkl'.format(self.epoch))
            csvSaver(path['backup'] + 'train_Loss_list', Loss_list)
            csvSaver(path['backup'] + 'train_Accuracy_list', Accuracy_list)
            csvSaver(path['backup'] + 'val_Accuracy_list', val_Accuracy_list)

    def validation(self, val_loader):
        total = 0
        correct = 0
        optimizer = self.optimizer
        with torch.no_grad():
            for batch_idx, (inputs, targets) in enumerate(val_loader):
                inputs, targets = inputs.to(device), targets.to(device)
                optimizer.zero_grad()  # 梯度歸零
                outputs = self.net(inputs)

                if triplet == True:
                    correct = tripletValidation(outputs, targets)
                    return correct

                _, predicted = torch.max(outputs.data, 1)
                total += targets.size(0)
                correct += predicted.eq(targets).sum().item()
            return np

    def test(self, test_loader):
        total = 0
        correct = 0
        optimizer = self.optimizer
        self.net.load_state_dict(torch.load('net_params601.pkl', map_location='cpu'))

        with torch.no_grad():
            for batch_idx, (inputs, targets) in enumerate(test_loader):
                print(batch_idx, targets)
                inputs, targets = inputs.to(device), targets.to(device)
                optimizer.zero_grad()  # 梯度歸零
                outputs = self.net(inputs)



            #     _, predicted = torch.max(outputs.data, 1)
            #     total += targets.size(0)
            #     correct += predicted.eq(targets).sum().item()
            # return correct * 100 / total



if __name__ == '__main__':
    if train == True:
        try:
            print(device)
            print(
                'initial_lr = {},'
                'epoch = {},'
                'batch_size = {},'
                'num_k_valid = {},'
                'k_valid = {},'
                'num_workers = {},'
                'random_seed = {},'.format(initial_lr,epoch,batch_size,num_k_valid,k_valid,num_workers,random_seed)
            )

            classes = num_classes(now_train_path)
            get_data = dataset(now_train_path, now_test_path)

            if k_valid == False:
                num_k_valid = 1

            for i in range(1, num_k_valid+1):
                run = main(epoch=epoch, n_class=classes)  # init 時才會初始化參數
                train_loader, val_loader = get_data.get_train_valid_loader(
                                                                  show_sample=False,
                                                                  batch_size=batch_size,
                                                                  random_seed=random_seed,
                                                                  k_valid=k_valid,
                                                                  num_k_valid=num_k_valid,
                                                                  multiplier_k_valid=i,
                                                                  num_workers=num_workers,
                                                                  pin_memory=True
                                                                  )
                run.training(train_loader=train_loader, val_loader=val_loader)
            print('Finished Training')

        except:
            # f = open('failRecord.txt', 'w')
            print(traceback.format_exc())
            # f.write(traceback.format_exc())
    else:
        get_data = dataset(now_train_path, now_test_path)
        test_loader = get_data.get_test_loader(batch_size=batch_size, shuffle=False)
        run = main(epoch=epoch, n_class=100)  # init 時才會初始化參數
        run.test(test_loader=test_loader)
