import numpy as np
import cv2
import model
import torch
from torchvision import transforms
from PIL import Image



faceCascade = cv2.CascadeClassifier('Cascades/haarcascade_frontalface_default.xml')
cap = cv2.VideoCapture(0)
cap.set(3,640)  # set Width
cap.set(4,480)  # set Height


class feature_analyse():
    def __init__(self):
        self.net = model.MobileNetV2(input_size=224)
        self.net.load_state_dict(torch.load('net_params2001.pkl', map_location='cpu'))
        self.normalize = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        self.transforms1 = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            # self.normalize,
        ])

    def analyse(self, img):
        img = self.transforms1(img)  # 預處理
        img = img.unsqueeze(0)  # 增加一維
        outputs = self.net(img)
        return outputs




if __name__ == '__main__':
    feature_analyse = feature_analyse()

    while True:
        ret, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  #灰階
        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.2,
            minNeighbors=5,
            minSize=(20, 20)
        )

        for (x, y, w, h) in faces:
            crop_img = img[y:y+h+5, x-6:x+w+6]
            crop_img_to_analyse = Image.fromarray(crop_img)  # 轉PIL

            cv2.imshow('video', crop_img)

        feature = feature_analyse.analyse(crop_img_to_analyse)
        print(feature)
        k = cv2.waitKey(10) & 0xff
        if k == 27:  # press 'ESC' to quit
            break

    cap.release()
    cv2.destroyAllWindows()
